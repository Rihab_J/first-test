#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <WiFiManager.h>
//Mqtt cloud information
const char* mqtt_server = "tailor.cloudmqtt.com";   /// example 192.168.0.19
const int  mqttPort = 12503 ;
const char* mqttUser = "qgfwvmkv";
const char* mqttPassword = "******";
 int i ;
WiFiClient espClient; //etablir une connection pour Ip et port spécifique
PubSubClient client(espClient); // recevoir comme entré les données de espcleint
WiFiManager wifiManager;
void setup() {
 
 Serial.begin(115200);
 WiFi.mode(WIFI_STA);
 wifiManager.autoConnect("AutoConnectAP");
 Serial.print("connecting");
  client.setServer(mqtt_server, mqttPort); // spécifié port et server 
  client.setCallback(callback);

  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");
 
    if (client.connect("ESP8266Client", mqttUser, mqttPassword )) {
 
      Serial.println("connected");  
 
    } else {
 
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
 
    }
  }
 
  client.publish("event", "bonjour a TOUS"); //to publish
  client.subscribe("event"); // pour recevoir a partir d'autres publishers
 
}
 
void callback(char* topic, byte* payload, unsigned int length) {
 pinMode(2, OUTPUT);
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
 
  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
 if ((char)payload[0]=='0')
 digitalWrite(2, HIGH);
 else digitalWrite(2, LOW);
 
  Serial.println();
  Serial.println("-----------------------");
 
}
 
void loop() {
  client.loop();
}
